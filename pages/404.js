import { useRouter } from "next/router";
import { useEffect } from "react";
import Link from "next/link";

import styles from '../styles/Home.module.css'

const NotFound = () => {
    const router = useRouter();
    useEffect( () => {
        setTimeout(() => {
            console.log('this useEffect is ranny');
            //router.go(-1/1);
            router.push('/');
        },3000)
    },[])
    return ( 
        <div className={styles.content}>
            <h1>Ooops...</h1>
            <h2>That page cannot be found :(</h2>
            <p>Going back to the <Link href="/"><a>Homepage</a></Link> is 3 seconds...</p>
        </div>
     );
}
 
export default NotFound;