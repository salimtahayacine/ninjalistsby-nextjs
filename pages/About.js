//import React from 'react';
import Head from 'next/head';
import styles from '../styles/Home.module.css';

const  About = () => {
  return (
    <div>
        <Head>
        <title>Ninja List | About</title>
        <meta name="keywords" content="ninjas"/>
        </Head>

        <div className={styles.content}>
            <h1 className={styles.title}>About</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa porro reiciendis ea quibusdam nemo nobis hic iure quos odio corporis, laudantium animi laborum voluptatem quia, veniam fuga aperiam officia sequi!</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa porro reiciendis ea quibusdam nemo nobis hic iure quos odio corporis, laudantium animi laborum voluptatem quia, veniam fuga aperiam officia sequi!</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa porro reiciendis ea quibusdam nemo nobis hic iure quos odio corporis, laudantium animi laborum voluptatem quia, veniam fuga aperiam officia sequi!</p>
        </div>
      </div>
  )
}

export default About;