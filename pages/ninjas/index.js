import styles from '../../styles/Home.module.css';
import Link from 'next/link';

export const getStaticProps = async () => {
    const res = await fetch('https://jsonplaceholder.typicode.com/users');
    const data = await res.json();

    return {
        props : { ninjas: data }
    }
}

const Ninjas = ({ ninjas }) => {
 console.log(ninjas);
    return ( 
        <div className='ninjasList'>
            <h1>All ninjas</h1>
            {ninjas.map(ninja => (
                <Link  className='ninjasList' href={'/ninjas/'+ninja.id} key={ninja.id}>
                    <a>
                        <h3>{ninja.name}</h3>
                    </a>
                </Link>
            ))}
        </div>
     );
}
 
export default Ninjas ;