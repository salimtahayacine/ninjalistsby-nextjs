import react from "react";
//import styles from ".../styles/globals.css";
export const getStaticPaths = async () => {
    
    const res = await fetch('https://jsonplaceholder.typicode.com/users');
    const data = await res.json();

    const paths = data.map( ninja => {
        return {
            params: { id: ninja.id.toString() }
        }
    })
    return {
        paths,
        fallback : false,
    }
}

export const getStaticProps = async (context) => {
    const id = context.params.id;
    const res = await fetch('https://jsonplaceholder.typicode.com/users/' + id);
    const data = await  res.json();

    return {
        props:{ ninja : data}
    }
}

const Details = ({ninja}) => {
    return ( 
        <div className="">
            <h1>Ninja Details :</h1>
            <div className="NinjasCard">
                <p>Name : {ninja.name}</p>
                <p>Email : {ninja.email}</p>
                <p>WebSite : {ninja.website}</p>
                <p>City : {ninja.address.city}</p>
            </div>
        </div>
     );
}
 
export default Details;